package eu4.comsim.gui;

import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import com.google.common.eventbus.Subscribe;

import eu4.comsim.core.Army;
import eu4.comsim.core.BattleInitRequest;
import eu4.comsim.core.Regiment;
import eu4.comsim.core.Technology;
import eu4.comsim.core.Util;
import eu4.comsim.core.datatypes.Unit;
import eu4.comsim.core.datatypes.UnitType;
import eu4.comsim.gui.TechnologyGroup.TechChangedEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * Composite containing all information relevant for one side of a battle.<br>
 * A {@link MainComposite} will contain two of these; one for the attacking and one for the defending side
 */
@Slf4j
public class ArmyGroup {
	
	private final String name;
	private final Army army;
	
	// Comboboxes for Unit - input will automatically be updated when
	// technology group or level is changed. Initialized in constructor
	Combo[] combos_unit = new Combo[3];
	// Controls number of regiments of each Unit
	Spinner[] spinners_unit = new Spinner[3];
	
	// Sub composites - General and Technology
	GeneralGroup group_general;
	TechnologyGroup group_tech;
	
	/**
	 * Create a new {@link ArmyGroup} composite.
	 *
	 * @param name The name used as the Group's text ("Attacker" or "Defender")
	 */
	public ArmyGroup(String name, Army army) {
		this.name = name;
		this.army = army;
		Util.EVENTBUS.register(this);
	}
	
	/**
	 * @wbp.factory
	 * @wbp.parser.entryPoint
	 */
	public static Group createGroup(Composite parent, String name) {
		return new ArmyGroup(name, Army.TEMPLATE_1).createGroup(parent);
		
	}
	
	/**
	 * @wbp.factory
	 * @wbp.parser.entryPoint
	 */
	public Group createGroup(Composite parent) {
		Group swtGroup = new Group(parent, SWT.NONE);
		swtGroup.setText(name);
		swtGroup.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		swtGroup.setLayout(new GridLayout(2, false));
		
		group_tech = new TechnologyGroup(swtGroup, army.getTechnology());
		Composite cp = new Composite(swtGroup, SWT.NONE);
		cp.setLayout(new GridLayout(1, false));
		group_general = new GeneralGroup(cp, army.getGeneral());
		
		Group grpRegiments = new Group(cp, SWT.NONE);
		grpRegiments.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpRegiments.setText("Regiments");
		GridLayout gl_grpRegiments = new GridLayout(3, false);
		grpRegiments.setLayout(gl_grpRegiments);
		
		for (UnitType type : UnitType.values()) {
			Label label_type = new Label(grpRegiments, SWT.NONE);
			label_type.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			label_type.setToolTipText(type.name);
			label_type.setImage(SWTResourceManager.getImage(MainComposite.class, type.imagePath));
			
			combos_unit[type.id] = new Combo(grpRegiments, SWT.READ_ONLY);
			combos_unit[type.id].setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
			combos_unit[type.id].addSelectionListener(new SelectionAdapter() {
				
				@SuppressWarnings("synthetic-access")
				@Override
				public void widgetSelected(SelectionEvent e) {
					String unitAsString = ((Combo) e.getSource()).getText();
					Unit unit = Unit.fromString(trimUnit(unitAsString));
					combos_unit[type.id].setToolTipText(unit.getTooltip());
					Util.EVENTBUS.post(BattleInitRequest.INSTANCE);
				}
			});
			
			Spinner spinner_type = new Spinner(grpRegiments, SWT.BORDER);
			spinner_type.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
			spinner_type.setSelection(type.getDefaultBataillonSize());
			spinners_unit[type.id] = spinner_type;
			spinners_unit[type.id].addModifyListener(e -> Util.EVENTBUS.post(BattleInitRequest.INSTANCE));
		}
		
		updateCombos(army.getTechnology());
		// Consider moving that to an initialization routine that is independent
		// of constructor
		fromRegiments(army.getRegiments());
		
		return swtGroup;
	}
	
	/**
	 * @return Collects general and regiments for all the input, and builds an {@link Army} from it.
	 */
	public Army buildArmy() {
		Technology tech = group_tech.parseTechnology();
		return new Army(group_general.getGeneral(), tech, getRegiments());
	}
	
	@Subscribe
	public void update(TechChangedEvent event) {
		log.debug("Got tech update");
		if (event.source.equals(group_tech))
			updateCombos(event.newTech);
	}
	
	/**
	 * The values of all the Unit combo boxes are updated
	 *
	 * Fills a {@link Combo} with {@link Unit}s of the given {@link UnitType} available at the given {@link Technology}
	 * level<br>
	 * The results will be sorted by tech level in descending order, so the most recent units are on top.
	 *
	 */
	public void updateCombos(Technology tech) {
		
		for (int i = 0; i < combos_unit.length; i++) {
			UnitType type = UnitType.values()[i];
			if (combos_unit[i] == null)
				return;
			// Predicate: Same UnitType, techlevel of Unit does not exceed mil
			// tech
			// level reached and techgroup must match (for non-artillery)
			Predicate<? super Unit> isSelectable = u -> (u.type == type && u.tech_level <= tech.getLevel().level
					&& ((u.type != UnitType.ARTILLERY) ? u.tech_group == tech.getGroup() : true));
			// Return all units that match the selection criteria, sort them
			// descending by tech level
			Stream<Unit> eligible = Stream.of(Unit.values()).filter(isSelectable)
					.sorted((u1, u2) -> Integer.compare(u2.tech_level, u1.tech_level));
			String[] names = eligible.map(u -> "[" + Integer.toString(u.tech_level) + "] " + u.getName())
					.toArray(String[]::new);
			if (names.length > 0) {
				combos_unit[i].setItems(names);
				combos_unit[i].setText(combos_unit[i].getItem(0));
				
				String trimmed = trimUnit(combos_unit[i].getText());
				combos_unit[i].setToolTipText(Unit.fromString(trimmed).getTooltip());
				combos_unit[i].setEnabled(true);
			} else {
				combos_unit[i].setItems(new String[] { "No unit available" });
				combos_unit[i].setText(combos_unit[i].getItem(0));
				combos_unit[i].setToolTipText("No unit available at this tech level");
				combos_unit[i].setEnabled(false);
			}
			Util.EVENTBUS.post(BattleInitRequest.INSTANCE);
		}
	}
	
	/**
	 * @return Reads the value of the {@link #combos_unit} boxes and creates a new set of regiments from it
	 */
	private List<Regiment> getRegiments() {
		
		Map<Unit, Integer> boilerPlate = new EnumMap<>(Unit.class);
		
		for (int i = 0; i < combos_unit.length; i++) {
			// "[10] Schwarze Reiter" -> "Schwarze Reiter"
			String trimmed = trimUnit(combos_unit[i].getText());
			if (Unit.fromString(trimmed) != null) {
				boilerPlate.put(Unit.fromString(trimmed), spinners_unit[i].getSelection());
			}
		}
		
		return Regiment.createRegiments(group_tech.parseTechnology(), boilerPlate);
		
	}
	
	/**
	 * TODO Merge with updateCombos
	 */
	private void fromRegiments(Collection<Regiment> regs) {
		for (int i = 0; i < UnitType.values().length; i++) {
			Collection<Regiment> contingent = Regiment.getRegiments(regs, UnitType.values()[i]);
			if (contingent.iterator().hasNext()) {
				Unit u = contingent.iterator().next().unit;
				combos_unit[i].setText("[" + Integer.toString(u.tech_level) + "] " + u.getName());
				spinners_unit[i].setSelection(contingent.size());
			} else {
				spinners_unit[i].setSelection(0);
			}
		}
	}
	
	private static String trimUnit(String unit) {
		return (unit.length() > 0) ? unit.substring(unit.indexOf(' ') + 1) : "No unit selected";
	}
}
