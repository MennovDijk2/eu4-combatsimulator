package eu4.comsim.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.eventbus.Subscribe;

import eu4.comsim.core.BattleStats;
import eu4.comsim.core.BattleUpdateEvent;
import eu4.comsim.core.Util;
import eu4.comsim.core.datatypes.UnitType;
import lombok.Getter;

/**
 * Displays results of a combat simulation.
 */
public class ResultsComposite {
	
	private boolean isAttacker;
	@Getter private DieMoraleComposite dma;
	/** A table displaying casualties, routed and fighting men, as well as percentage information */
	Table table;
	
	public ResultsComposite(boolean attacker) {
		this.isAttacker = attacker;
		Util.EVENTBUS.register(this);
	}
	
	/**
	 * @wbp.factory Create the composite and initialize its components
	 */
	public Composite createResultsComposite(Composite parent, int style) {
		Composite composite = new Composite(parent, style);
		composite.setToolTipText("Change to last simulation");
		composite.setLayout(new GridLayout(1, true));
		
		composite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		
		GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1);
		gd_composite.widthHint = 280;
		gd_composite.heightHint = 100;
		dma = new DieMoraleComposite(isAttacker);
		Composite dmComp = dma.createComposite(composite, gd_composite);
		
		table = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1);
		gd_table.widthHint = 280;
		table.setLayoutData(gd_table);
		table.setHeaderVisible(true);
		
		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setWidth(62);
		TableColumn tblclmnCasualties = new TableColumn(table, SWT.NONE);
		tblclmnCasualties.setWidth(70);
		tblclmnCasualties.setText("Casualties");
		
		TableColumn tblclmnRouted = new TableColumn(table, SWT.NONE);
		tblclmnRouted.setWidth(70);
		tblclmnRouted.setText("Routed");
		
		for (UnitType type : UnitType.values()) {
			TableItem tableItem = new TableItem(table, SWT.NONE);
			tableItem.setText(type.name);
		}
		
		TableItem tableItem_Total = new TableItem(table, SWT.NONE);
		tableItem_Total.setText("Total");
		
		TableColumn tblclmnFighting = new TableColumn(table, SWT.NONE);
		tblclmnFighting.setToolTipText("Men on battlefield (average morale)");
		tblclmnFighting.setWidth(70);
		tblclmnFighting.setText("Fighting");
		
		TableItem tableItem_Percent = new TableItem(table, SWT.NONE);
		tableItem_Percent.setText("Percent");
		
		return composite;
	}
	
	@Subscribe
	public void update(BattleUpdateEvent o) {
		BattleStats stats = o.getStats(isAttacker);
		
		double casualties = stats.casualties();
		double routed = stats.routed();
		double fighting = stats.fighting();
		double originalStrength = stats.originalStrength();
		
		Util.DF.setMinimumFractionDigits(2);
		Util.DF.setMaximumFractionDigits(2);
		
		for (int i = 0; i <= 2; i++) {
			UnitType type = UnitType.values()[i];
			double casualtiesForType = stats.adaptTo(type).casualties();
			double routedForType = stats.adaptTo(type).routed();
			double fightingForType = stats.adaptTo(type).fighting();
			double averageMoraleForType = stats.adaptTo(type).averageMorale();
			table.getItem(i).setText(1, Util.IF.format(casualtiesForType));
			table.getItem(i).setText(2, Util.IF.format(routedForType));
			table.getItem(i).setText(3,
					Util.IF.format(fightingForType) + " (" + Util.DF.format(averageMoraleForType) + ")");
		}
		
		table.getItem(3).setText(1, Util.IF.format(casualties));
		table.getItem(3).setText(2, Util.IF.format(routed));
		table.getItem(3).setText(3, Util.IF.format(fighting));
		
		table.getItem(4).setText(1, Util.PF.format(casualties / originalStrength));
		table.getItem(4).setText(2, Util.PF.format(routed / originalStrength));
		table.getItem(4).setText(3, Util.PF.format(fighting / originalStrength));
		
	}
}
