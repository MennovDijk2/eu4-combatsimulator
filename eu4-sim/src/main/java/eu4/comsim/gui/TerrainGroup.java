package eu4.comsim.gui;

import java.util.stream.Stream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.Terrain.CrossingPenalty;
import lombok.extern.slf4j.Slf4j;

/**
 * Contains UI elements regarding choice of {@link Terrain} and potential attacker penalties from it (strait, river,
 * landing ...)
 *
 */
@Slf4j
public class TerrainGroup {
	
	public static final int DEFAULT_IMG_WIDTH = 300;
	public static final int DEFAULT_IMG_HEIGHT = 70;
	
	Terrain selectedTerrain;
	CrossingPenalty crossingPenalty;
	
	/**
	 * Create the composite.
	 */
	public TerrainGroup(Terrain terrain, CrossingPenalty cp) {
		selectedTerrain = terrain;
		crossingPenalty = cp;
	}
	
	/**
	 * @wbp.factory
	 * @wbp.parser.entryPoint
	 */
	public Group createGroup(Composite parent) {
		Group swtGroup = new Group(parent, SWT.NONE);
		swtGroup.setText("Terrain");
		swtGroup.setLayout(new GridLayout(1, false));
		
		GridData gd_trngrpTerrain = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_trngrpTerrain.widthHint = 350;
		gd_trngrpTerrain.heightHint = 80;
		swtGroup.setLayoutData(gd_trngrpTerrain);
		
		Button button_terrain = new Button(swtGroup, SWT.PUSH);
		GridData gd_button_terrain = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_button_terrain.heightHint = 80;
		gd_button_terrain.widthHint = 300;
		button_terrain.setLayoutData(gd_button_terrain);
		button_terrain.setToolTipText("Right-click to choose terrain");
		Image i = SWTResourceManager.getImage(TerrainGroup.class, selectedTerrain.imagePath);
		i = new Image(Display.getDefault(), i.getImageData().scaledTo(DEFAULT_IMG_WIDTH, DEFAULT_IMG_HEIGHT));
		button_terrain.setImage(i);
		
		Menu menu_chooseTerrain = new Menu(button_terrain);
		for (Terrain t : Terrain.values()) {
			MenuItem item = new MenuItem(menu_chooseTerrain, SWT.RADIO);
			item.setText(t.name);
			item.setSelection(t == selectedTerrain);
			item.addSelectionListener(new SelectionAdapter() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					Image tImg = SWTResourceManager.getImage(TerrainGroup.class,
							Terrain.fromString(item.getText()).imagePath);
					tImg = new Image(Display.getDefault(),
							tImg.getImageData().scaledTo(DEFAULT_IMG_WIDTH, DEFAULT_IMG_HEIGHT));
					button_terrain.setImage(tImg);
					selectedTerrain = Terrain.fromString(item.getText());
				}
			});
		}
		button_terrain.setMenu(menu_chooseTerrain);
		
		Combo combo_attackerPenalty = new Combo(swtGroup, SWT.READ_ONLY);
		combo_attackerPenalty
				.setItems(Stream.of(Terrain.CrossingPenalty.values()).map(t -> t.name).toArray(String[]::new));
		combo_attackerPenalty.select(crossingPenalty.ordinal());
		combo_attackerPenalty.addSelectionListener(new SelectionAdapter() {
			
			@SuppressWarnings("synthetic-access")
			@Override
			public void widgetSelected(SelectionEvent e) {
				log.debug("Selected index: {}, selected item: {}, text content in the text field: {}",
						combo_attackerPenalty.getSelectionIndex(),
						combo_attackerPenalty.getItem(combo_attackerPenalty.getSelectionIndex()),
						combo_attackerPenalty.getText());
				crossingPenalty = Terrain.CrossingPenalty.values()[combo_attackerPenalty.getSelectionIndex()];
			}
		});
		combo_attackerPenalty.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		return swtGroup;
	}
	
	public Terrain.CrossingPenalty getPenalty() {
		return crossingPenalty;
	}
	
	public Terrain getTerrain() {
		return selectedTerrain;
	}
}
