package eu4.comsim.core;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.UnitType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;

/**
 * Represents a nation's technology in EU4.
 */
@Value
public class Technology implements Serializable {
	
	private static final long serialVersionUID = 8202160232359736334L;
	
	/**
	 * Technology group
	 */
	@AllArgsConstructor
	public enum Group {
		OTTOMAN("Anatolian", 0.5, "/icons/tech/Ottoman.png"),
		CHINESE("Chinese", 0.5, "/icons/tech/Chinese.png"),
		EASTERN("Eastern", 0.6, "/icons/tech/Eastern.png"),
		/**
		 * "Fantasy" tech group, appears only under very special circumstances
		 */
		// HIGHAMERICAN("High-American", 0.5, "/icons/tech/High_American.png"),
		INDIAN("Indian", 0.5, "/icons/tech/Indian.png"),
		MESOAMERICAN("Meso-American", 0.5, "/icons/tech/Mesoamerican.png"),
		MUSLIM("Muslim", 0.8, "/icons/tech/Muslim.png"),
		NOMAD("Nomadic", 1.0, "/icons/tech/Nomadic.png"),
		NORTH_AMERICAN("North-American", 0.5, "/icons/tech/North_American.png"),
		SOUTH_AMERICAN("South-American", 0.5, "/icons/tech/South_American.png"),
		SUB_SAHARAN("Sub-Saharan", 0.5, "/icons/tech/Sub-Saharan.png"),
		WESTERN("Western", 0.5, "/icons/tech/Western.png");
		
		public final String name;
		/**
		 * Determines potential insufficient support penalty
		 *
		 * @see <a href=
		 *      "http://www.eu4wiki.com/Land_warfare#Insufficient_support">http:
		 *      //www.eu4wiki.com/Land_warfare#Insufficient_support</a>
		 */
		public final double maxCavalryRatio;
		public final String imagePath;
		
		/**
		 * Convenience method to find a Technology.Group of the given name. If no Group
		 * of the exact same name exists, <code>null</code> is returned
		 */
		public static Group fromString(String name) {
			return Stream.of(Group.values()).filter(g -> g.name.equalsIgnoreCase(name)).findAny().orElse(null);
		}
	}
	
	@AllArgsConstructor
	@ToString
	public enum MilTechModifier {
		TECH_001(1, 2.00, 0.50, 15, 1.00, 0.35, 0.30, 0.00, 0.80, 0.00, 0.00),
		TECH_002(2, 2.00, 0.50, 20, 1.00, 0.35, 0.50, 0.00, 1.00, 0.00, 0.00),
		TECH_003(3, 2.50, 0.50, 20, 1.00, 0.35, 0.50, 0.00, 1.00, 0.00, 0.00),
		TECH_004(4, 3.00, 0.75, 20, 1.00, 0.35, 0.50, 0.00, 1.00, 0.00, 0.00),
		TECH_005(5, 3.00, 0.75, 22, 1.00, 0.35, 0.65, 0.00, 1.20, 0.00, 0.00),
		TECH_006(6, 3.00, 1.00, 24, 1.00, 0.55, 0.95, 0.00, 1.20, 0.00, 0.00),
		TECH_007(7, 3.00, 1.25, 24, 1.00, 0.55, 0.95, 0.00, 1.20, 1.00, 0.05),
		TECH_008(8, 3.00, 1.25, 24, 1.00, 0.80, 0.95, 0.00, 2.00, 1.00, 0.05),
		TECH_009(9, 3.00, 1.50, 25, 1.00, 0.80, 0.95, 0.00, 2.00, 1.00, 0.05),
		TECH_10(10, 3.00, 1.50, 25, 1.25, 0.80, 0.95, 0.00, 2.00, 1.00, 0.05),
		TECH_11(11, 3.00, 1.50, 27, 1.25, 0.80, 1.15, 0.50, 2.00, 1.00, 0.05),
		TECH_12(12, 3.00, 1.75, 27, 1.25, 0.80, 1.15, 0.50, 2.00, 1.00, 0.05),
		TECH_13(13, 3.00, 1.75, 27, 1.25, 0.80, 1.15, 0.50, 2.00, 1.00, 0.15),
		TECH_14(14, 3.00, 1.75, 29, 1.25, 1.10, 1.15, 0.50, 2.00, 1.00, 0.15),
		TECH_15(15, 4.00, 2.00, 29, 1.25, 1.10, 1.15, 0.50, 2.00, 1.00, 0.15),
		TECH_16(16, 4.00, 2.00, 30, 1.25, 1.10, 1.15, 0.50, 2.00, 2.40, 0.25),
		TECH_17(17, 4.00, 2.00, 30, 1.25, 1.10, 1.15, 0.50, 3.00, 2.40, 0.25),
		TECH_18(18, 4.00, 2.00, 32, 1.50, 1.10, 1.15, 0.50, 3.00, 2.40, 0.25),
		TECH_19(19, 4.00, 2.50, 32, 1.50, 1.10, 1.15, 0.50, 3.00, 2.40, 0.25),
		TECH_20(20, 4.00, 2.50, 34, 1.50, 1.60, 1.15, 0.50, 3.00, 2.40, 0.25),
		TECH_21(21, 4.00, 2.75, 34, 1.50, 1.60, 1.65, 0.50, 3.00, 2.40, 0.25),
		TECH_22(22, 4.00, 2.75, 36, 1.50, 1.60, 1.65, 1.00, 3.00, 4.40, 0.35),
		TECH_23(23, 4.00, 3.00, 36, 2.00, 1.60, 1.65, 1.00, 4.00, 4.40, 0.35),
		TECH_24(24, 4.00, 3.25, 38, 2.00, 1.60, 1.65, 1.00, 4.00, 4.40, 0.35),
		TECH_25(25, 4.00, 3.25, 38, 2.00, 1.60, 1.65, 1.00, 4.00, 6.40, 0.45),
		TECH_26(26, 5.00, 3.25, 40, 2.00, 1.60, 1.65, 1.00, 4.00, 6.40, 0.45),
		TECH_27(27, 5.00, 3.25, 40, 2.00, 2.10, 1.65, 1.00, 4.00, 6.40, 0.45),
		TECH_28(28, 5.00, 3.25, 40, 2.25, 2.10, 2.15, 1.00, 4.00, 6.40, 0.45),
		TECH_29(29, 5.00, 3.25, 40, 2.25, 2.10, 2.15, 1.00, 4.00, 6.40, 0.45),
		TECH_30(30, 6.00, 3.50, 40, 2.50, 3.10, 2.15, 1.00, 4.00, 6.40, 0.45),
		TECH_31(31, 6.00, 3.50, 40, 2.50, 3.10, 2.15, 1.00, 5.00, 6.40, 0.45),
		TECH_32(32, 6.00, 3.75, 40, 2.50, 3.10, 2.15, 1.00, 5.00, 8.40, 0.55);
		
		public final int level;
		public final double morale;
		public final double tactics;
		public final int combatWidth;
		public final double flankingRange;
		public final double infantryFire;
		public final double infantryShock;
		public final double cavalryFire;
		public final double cavalryShock;
		public final double artilleryFire;
		public final double artilleryShock;
		
		public String getTooltip() {
			String[] techLevelDescription = toString().replace(")", "").split(",");
			List<String> trimmed = Arrays.asList(techLevelDescription).subList(1, techLevelDescription.length);
			return String.join(System.getProperty("line.separator"), trimmed);
		}
	}
	
	private double discipline;
	private double moraleModifier;
	
	private double infCombatAbility, cavCombatAbility, artCombatAbility;
	
	private MilTechModifier level;
	@Getter private Group group;
	
	public Technology(int level) {
		this(level, Group.WESTERN, 1.0, 1.0, 1.0, 1.0, 1.0);
	}
	
	/**
	 * Self explanatory.
	 */
	public Technology(int level, Group group, double discipline, double moraleModifier, double infCA, double cavCA, double artCA) {
		this.level = MilTechModifier.values()[level - 1];
		this.group = group;
		
		this.discipline = discipline;
		this.moraleModifier = moraleModifier;
		this.infCombatAbility = infCA;
		this.cavCombatAbility = cavCA;
		this.artCombatAbility = artCA;
	}
	
	public double casualtyMultiplier(UnitType unit, Phase phase) {
		return getCombatModifier(phase, unit) * getCombatAbility(unit) * discipline;
	}
	
	public double getMorale() {
		return level.morale * moraleModifier;
	}
	
	public double getTactics() {
		return level.tactics * discipline;
	}
	
	private double getCombatAbility(UnitType type) {
		switch (type) {
		case INFANTRY:
			return infCombatAbility;
		case CAVALRY:
			return cavCombatAbility;
		case ARTILLERY:
			return artCombatAbility;
		default:
			return 1.0;
		}
	}
	
	private double getCombatModifier(Phase phase, UnitType type) {
		if (phase == Phase.FIRE) {
			if (type == UnitType.INFANTRY)
				return level.infantryFire;
			if (type == UnitType.CAVALRY)
				return level.cavalryFire;
			if (type == UnitType.ARTILLERY)
				return level.artilleryFire;
		} else if (phase == Phase.SHOCK) {
			if (type == UnitType.INFANTRY)
				return level.infantryShock;
			if (type == UnitType.CAVALRY)
				return level.cavalryShock;
			if (type == UnitType.ARTILLERY)
				return level.artilleryShock;
		} else {
			return getMorale();
		}
		
		throw new IllegalArgumentException("Unknown combat modifier");
	}
	
	@Override
	public String toString() {
		return level.level + " (" + group.name + ")";
	}
}
