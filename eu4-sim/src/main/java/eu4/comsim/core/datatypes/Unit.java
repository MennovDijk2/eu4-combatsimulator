package eu4.comsim.core.datatypes;

import static eu4.comsim.core.Technology.Group.CHINESE;
import static eu4.comsim.core.Technology.Group.EASTERN;
import static eu4.comsim.core.Technology.Group.INDIAN;
import static eu4.comsim.core.Technology.Group.MESOAMERICAN;
import static eu4.comsim.core.Technology.Group.MUSLIM;
import static eu4.comsim.core.Technology.Group.NOMAD;
import static eu4.comsim.core.Technology.Group.NORTH_AMERICAN;
import static eu4.comsim.core.Technology.Group.OTTOMAN;
import static eu4.comsim.core.Technology.Group.SOUTH_AMERICAN;
import static eu4.comsim.core.Technology.Group.SUB_SAHARAN;
import static eu4.comsim.core.Technology.Group.WESTERN;
import static eu4.comsim.core.datatypes.UnitType.ARTILLERY;
import static eu4.comsim.core.datatypes.UnitType.CAVALRY;
import static eu4.comsim.core.datatypes.UnitType.INFANTRY;

import java.util.stream.Stream;

import eu4.comsim.core.Technology;
import eu4.comsim.gui.TechnologyGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enumeration of all Units unlockable in the game.<br>
 *
 * @see <a href="http://www.eu4wiki.com/Land_units">http://www.eu4wiki.com/
 *      Land_units</a>
 */
@AllArgsConstructor
public enum Unit {
	
	ADAL_GUERILLA_WARFARE(23, SUB_SAHARAN, INFANTRY, "East African Guerrillas", 3, 3, 2, 2, 2, 3),
	ADAL_GUNPOWDER_WARFARE(15, SUB_SAHARAN, INFANTRY, "East African Musketeers", 2, 2, 2, 2, 2, 3),
	AFRICAN_ABYSSINIAN_CAVALRY(10, SUB_SAHARAN, CAVALRY, "Abyssinian Barded Cavalry", 0, 0, 3, 2, 3, 2),
	AFRICAN_ABYSSINIAN_LIGHT_CAVALRY(1, SUB_SAHARAN, CAVALRY, "Abyssinian Light Cavalry", 0, 0, 1, 1, 2, 0),
	AFRICAN_CLUBMEN(1, SUB_SAHARAN, INFANTRY, "African Clubmen", 0, 0, 1, 0, 1, 1),
	AFRICAN_CUIRASSIER(28, SUB_SAHARAN, CAVALRY, "African Cuirassier", 1, 1, 4, 4, 4, 4),
	AFRICAN_DRAGOON(23, SUB_SAHARAN, CAVALRY, "African Dragoon", 1, 1, 3, 3, 4, 4),
	AFRICAN_HILL_WARFARE(12, SUB_SAHARAN, INFANTRY, "African Hill Warriors", 2, 1, 1, 2, 2, 2),
	AFRICAN_HUSSAR(14, SUB_SAHARAN, CAVALRY, "African Hussar", 1, 0, 3, 2, 3, 3),
	AFRICAN_MANDELAKU(1, SUB_SAHARAN, CAVALRY, "Mandekalu Cavalry", 0, 0, 1, 1, 1, 1),
	AFRICAN_MOSSI_HORSEMEN(10, SUB_SAHARAN, CAVALRY, "Mossi Horsemen", 0, 0, 2, 2, 3, 3),
	AFRICAN_SOMALI_CAVALRY(6, SUB_SAHARAN, CAVALRY, "Somali Light Cavalry", 0, 0, 2, 1, 1, 2),
	AFRICAN_SPEARMEN(1, SUB_SAHARAN, INFANTRY, "African Spearmen", 0, 0, 0, 1, 1, 1),
	AFRICAN_SWARM(17, SUB_SAHARAN, CAVALRY, "African Swarm Cavalry", 0, 1, 4, 3, 3, 2),
	AFRICAN_TUAREG_CAVALRY(6, SUB_SAHARAN, CAVALRY, "Tuareg Cavalry", 0, 0, 2, 1, 2, 1),
	AFRICAN_WESTERN_FRANCHISE_WARFARE(30, SUB_SAHARAN, INFANTRY, "African Western Franchise Infantry", 3, 3, 3, 3, 4, 4),
	AFSHARID_REFORMED(18, MUSLIM, CAVALRY, "Afsharid Reformed Cavalry", 0, 1, 4, 3, 3, 3),
	AFSHARID_REFORMED_INFANTRY(15, MUSLIM, INFANTRY, "Afsharid Reformed Infantry", 3, 2, 2, 2, 2, 2),
	ALGONKIN_TOMAHAWK_CHARGE(5, NORTH_AMERICAN, INFANTRY, "Algonkin Tomahawk Warriors", 0, 0, 2, 1, 2, 1),
	ANGLOFRENCH_LINE(23, WESTERN, INFANTRY, "Line Infantry", 3, 3, 2, 2, 3, 3),
	APACHE_GUERILLA(26, NORTH_AMERICAN, INFANTRY, "North American Guerrillas", 4, 3, 2, 4, 3, 3),
	ASIAN_ARQUEBUSIER(9, CHINESE, INFANTRY, "Asian Arquebusier", 1, 1, 1, 1, 1, 1),
	ASIAN_CHARGE_CAVALRY(14, CHINESE, CAVALRY, "Asian Charge Cavalry", 0, 1, 3, 2, 3, 3),
	ASIAN_MASS_INFANTRY(15, CHINESE, INFANTRY, "Asian Mass Infantry", 3, 2, 2, 2, 2, 2),
	ASIAN_MUSKETEER(19, CHINESE, INFANTRY, "Asian Musketeer", 3, 3, 2, 2, 3, 2),
	AUSTRIAN_GRENZER(23, WESTERN, INFANTRY, "Grenzer Infantry", 2, 3, 2, 3, 3, 3),
	AUSTRIAN_HUSSAR(23, WESTERN, CAVALRY, "Latin Hussars", 1, 2, 4, 3, 4, 4),
	AUSTRIAN_JAEGER(30, WESTERN, INFANTRY, "Jaeger Infantry", 4, 4, 4, 3, 3, 4),
	AUSTRIAN_TERCIO(19, WESTERN, INFANTRY, "Reformed Tercio", 2, 2, 2, 3, 3, 3),
	AUSTRIAN_WHITE_COAT(26, WESTERN, INFANTRY, "White Coat Infantry", 4, 3, 3, 3, 3, 4),
	AZTEC_GUNPOWDER_WARFARE(14, MESOAMERICAN, INFANTRY, "American Hill Musketeers", 2, 1, 3, 2, 2, 2),
	AZTEC_HILL_WARFARE(10, MESOAMERICAN, INFANTRY, "Reformed American Hill Warriors", 0, 0, 3, 1, 2, 2),
	AZTEC_TRIBAL_WARFARE(5, MESOAMERICAN, INFANTRY, "American Hill Warriors", 0, 0, 2, 1, 2, 1),
	BANTU_GUNPOWDER_WARFARE(15, SUB_SAHARAN, INFANTRY, "South African Musketeers", 3, 2, 3, 2, 2, 1),
	BANTU_PLAINS_WARFARE(12, SUB_SAHARAN, INFANTRY, "African Plains Warriors", 2, 1, 2, 2, 2, 1),
	BANTU_TRIBAL_WARFARE(5, SUB_SAHARAN, INFANTRY, "South African Warrior", 0, 0, 2, 1, 2, 1),
	BARDICHE_INFANTRY(1, EASTERN, INFANTRY, "Bardiche Infantry", 0, 0, 0, 0, 1, 1),
	BHONSLE_CAVALRY(28, INDIAN, CAVALRY, "Sowars", 1, 2, 4, 4, 4, 3),
	BHONSLE_INFANTRY(18, INDIAN, INFANTRY, "Deccani Musket Infantry", 3, 3, 2, 2, 2, 2),
	BRITISH_HUSSAR(26, WESTERN, CAVALRY, "Reformed Latin Hussars", 1, 2, 4, 4, 4, 4),
	BRITISH_REDCOAT(26, WESTERN, INFANTRY, "Redcoat Infantry", 3, 4, 3, 3, 4, 3),
	BRITISH_SQUARE(28, WESTERN, INFANTRY, "Square Infantry", 3, 4, 3, 3, 4, 4),
	CENTRAL_AMERICAN_DRAGOON(23, MESOAMERICAN, CAVALRY, "Central American Dragoon", 2, 2, 3, 3, 4, 4),
	CENTRAL_AMERICAN_HORSEMEN(6, MESOAMERICAN, CAVALRY, "Central American Horsemen", 0, 0, 2, 1, 1, 1),
	CENTRAL_AMERICAN_HUSSAR(14, MESOAMERICAN, CAVALRY, "Central American Hussar", 1, 1, 3, 2, 3, 2),
	CENTRAL_AMERICAN_RIFLE_CAVALRY(10, MESOAMERICAN, CAVALRY, "Central American Cavalry", 0, 1, 3, 2, 2, 2),
	CENTRAL_AMERICAN_SWARM(19, MESOAMERICAN, CAVALRY, "Central American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
	CHAMBERED_DEMI_CANNON(16, WESTERN, ARTILLERY, "Chambered Demi-Cannon", 2, 2, 0, 1, 2, 1),
	CHEVAUCHEE(1, WESTERN, CAVALRY, "Chevauchée", 0, 0, 1, 1, 1, 0),
	CHINESE_DRAGOON(23, CHINESE, CAVALRY, "Asian Dragoons", 1, 2, 3, 3, 4, 3),
	CHINESE_FOOTSOLDIER(5, CHINESE, INFANTRY, "Offensive Asian Foot Soldier", 0, 0, 1, 0, 2, 1),
	CHINESE_LONGSPEAR(1, CHINESE, INFANTRY, "Asian Longspear Infantry", 0, 0, 0, 1, 0, 1),
	CHINESE_STEPPE(6, CHINESE, CAVALRY, "Asian Steppe Cavalry", 0, 0, 2, 2, 1, 1),
	COEHORN_MORTAR(22, WESTERN, ARTILLERY, "Coehorn Mortar", 3, 3, 1, 1, 3, 3),
	COMMANCHE_SWARM(19, NORTH_AMERICAN, CAVALRY, "North American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
	CREEK_ARQUEBUSIER(14, NORTH_AMERICAN, INFANTRY, "Creek Arquebusier", 2, 2, 2, 2, 2, 2),
	CULVERIN(10, WESTERN, ARTILLERY, "Culverin", 1, 1, 0, 1, 1, 0),
	DRUZHINA_CAVALRY(1, EASTERN, CAVALRY, "Druzhina Cavalry", 0, 0, 0, 1, 1, 1),
	DURRANI_DRAGOON(28, MUSLIM, CAVALRY, "Durrani Cavalry", 2, 1, 5, 3, 4, 4),
	DURRANI_RIFLED_MUSKETEER(23, MUSLIM, INFANTRY, "Reformed Muslim Musketeers", 1, 2, 3, 3, 4, 3),
	DURRANI_SWIVEL(28, MUSLIM, CAVALRY, "Durrani Swivel Cavalry", 1, 2, 4, 5, 4, 3),
	DUTCH_MAURICIAN(15, WESTERN, INFANTRY, "Maurician Infantry", 2, 2, 2, 1, 3, 2),
	EAST_ASIAN_SPEARMEN(1, CHINESE, INFANTRY, "East Asian Spearmen", 0, 0, 0, 1, 1, 0),
	EAST_MONGOLIAN_STEPPE(10, CHINESE, CAVALRY, "Reformed Asian Steppe Cavalry", 1, 0, 2, 2, 2, 2),
	EASTERN_BOW(1, CHINESE, CAVALRY, "East Asian Archer Cavalry", 0, 0, 1, 0, 1, 1),
	EASTERN_CARABINIER(26, OTTOMAN, INFANTRY, "Eastern Carabinier", 4, 3, 3, 3, 3, 3),
	EASTERN_KNIGHTS(1, EASTERN, CAVALRY, "Eastern Knights", 0, 0, 1, 0, 1, 1),
	EASTERN_MEDIEVAL_INFANTRY(1, EASTERN, INFANTRY, "Eastern Medieval Infantry", 0, 0, 1, 0, 1, 0),
	EASTERN_MILITIA(5, EASTERN, INFANTRY, "Eastern Militia", 0, 0, 1, 1, 1, 1),
	EASTERN_SKIRMISHER(26, OTTOMAN, CAVALRY, "Eastern Skirmisher", 1, 2, 4, 3, 4, 3),
	EASTERN_UHLAN(26, OTTOMAN, CAVALRY, "Eastern Uhlan", 2, 1, 3, 4, 4, 3),
	ETHIOPIAN_GUERILLA_WARFARE(23, SUB_SAHARAN, INFANTRY, "North African Guerrillas", 2, 3, 2, 3, 2, 3),
	ETHIOPIAN_GUNPOWDER_WARFARE(15, SUB_SAHARAN, INFANTRY, "North African Musketeers", 2, 3, 2, 3, 1, 2),
	ETHIOPIAN_MOUNTAIN_WARFARE(12, SUB_SAHARAN, INFANTRY, "African Mountain Warriors", 1, 2, 2, 2, 1, 2),
	FLYING_BATTERY(29, WESTERN, ARTILLERY, "Flying Battery", 4, 4, 2, 2, 4, 4),
	FRENCH_BLUECOAT(26, WESTERN, INFANTRY, "Blue Coat Infantry", 3, 3, 3, 3, 4, 4),
	FRENCH_CARABINIER(26, WESTERN, CAVALRY, "Carabiniers", 2, 1, 5, 4, 3, 4),
	FRENCH_CARACOLLE(14, WESTERN, CAVALRY, "Latin Caracole Cavalry", 1, 0, 3, 2, 2, 2),
	FRENCH_CUIRASSIER(28, WESTERN, CAVALRY, "Latin Cuirassiers", 1, 2, 5, 4, 4, 5),
	FRENCH_DRAGOON(23, WESTERN, CAVALRY, "Latin Dragoons", 1, 1, 4, 4, 4, 4),
	FRENCH_IMPULSE(28, WESTERN, INFANTRY, "Impulse Infantry", 4, 3, 3, 3, 4, 4),
	GAELIC_FREE_SHOOTER(12, WESTERN, INFANTRY, "Free Shooter Infantry", 1, 2, 2, 1, 3, 1),
	GAELIC_GALLOGLAIGH(5, WESTERN, INFANTRY, "Galloglaigh Infantry", 0, 0, 1, 0, 2, 0),
	GAELIC_MERCENARY(9, WESTERN, INFANTRY, "Reformed Galloglaigh Infantry", 0, 0, 2, 0, 2, 1),
	GERMANIZED_PIKE(9, EASTERN, INFANTRY, "Pike Infantry", 0, 0, 1, 2, 1, 2),
	HALBERD_INFANTRY(1, WESTERN, INFANTRY, "Halberd Infantry", 0, 0, 1, 0, 1, 0),
	HAN_BANNER(12, CHINESE, INFANTRY, "Banner Infantry", 1, 2, 2, 1, 2, 2),
	HOUFNICE(7, WESTERN, ARTILLERY, "Houfnice", 1, 0, 0, 0, 0, 1),
	HUNGARIAN_HUSSAR(10, EASTERN, CAVALRY, "Eastern Hussar", 0, 0, 2, 2, 3, 2),
	HURON_ARQUEBUSIER(14, NORTH_AMERICAN, INFANTRY, "Native American Arquebusier", 2, 2, 2, 1, 3, 2),
	INCA_MOUNTAIN_WARFARE(5, SOUTH_AMERICAN, INFANTRY, "Reformed Mountain Warriors", 0, 0, 1, 2, 1, 2),
	INCAN_AXEMEN(5, SOUTH_AMERICAN, INFANTRY, "Incan Axemen", 0, 0, 1, 1, 2, 2),
	INCAN_GUERILLA_WARFARE(19, SOUTH_AMERICAN, INFANTRY, "Incan Guerrilla", 2, 3, 2, 2, 2, 3),
	INCAN_SLINGSHOTS(5, SOUTH_AMERICAN, INFANTRY, "Incan Slingshots", 0, 0, 2, 2, 1, 1),
	INDIAN_ARCHERS(1, INDIAN, CAVALRY, "Elephant Archers", 0, 0, 1, 0, 1, 1),
	INDIAN_ARQUEBUSIER(5, INDIAN, INFANTRY, "Indian Arquebusier", 1, 0, 1, 1, 1, 0),
	INDIAN_ELEPHANT(10, INDIAN, CAVALRY, "Mansabdar Cavalry", 1, 0, 3, 2, 2, 2),
	INDIAN_FOOTSOLDIER(1, INDIAN, INFANTRY, "Indian Foot Soldier", 0, 0, 1, 0, 1, 1),
	INDIAN_RIFLE(26, INDIAN, INFANTRY, "North Indian Sepoy", 3, 3, 3, 3, 3, 4),
	INDIAN_SHOCK_CAVALRY(23, INDIAN, CAVALRY, "Deccani Light Cavalry", 1, 1, 4, 3, 4, 3),
	IRISH_CHARGE(15, WESTERN, INFANTRY, "Charge Infantry", 1, 2, 3, 1, 3, 2),
	IROQUOIS_RIFLE_SCOUT(19, NORTH_AMERICAN, INFANTRY, "Rifle Scout Infantry", 3, 3, 2, 2, 2, 2),
	ITALIAN_CONDOTTA(9, WESTERN, INFANTRY, "Condotta Infantry", 0, 0, 2, 1, 1, 1),
	JAPANESE_ARCHER(1, CHINESE, INFANTRY, "Asian Longbow", 0, 0, 1, 0, 0, 1),
	JAPANESE_FOOTSOLDIER(5, CHINESE, INFANTRY, "Defensive Asian Foot Soldier", 0, 0, 1, 1, 1, 1),
	JAPANESE_SAMURAI(6, CHINESE, CAVALRY, "Samurai Cavalry", 0, 0, 2, 1, 2, 1),
	LARGE_CAST_BRONZE_MORTAR(7, WESTERN, ARTILLERY, "Large Cast Bronze Mortar", 1, 0, 0, 0, 1, 0),
	LARGE_CAST_IRON_BOMBARD(13, WESTERN, ARTILLERY, "Large Cast Iron Cannon", 2, 1, 0, 0, 1, 2),
	LEATHER_CANNON(18, WESTERN, ARTILLERY, "Leather Cannon", 2, 2, 1, 1, 2, 2),
	MAHARATHAN_CAVALRY(17, INDIAN, CAVALRY, "Maratha Raiders", 0, 2, 4, 3, 3, 2),
	MAHARATHAN_GUERILLA_WARFARE(23, INDIAN, INFANTRY, "Telingas", 4, 3, 2, 2, 2, 3),
	MALI_TRIBAL_WARFARE(5, SUB_SAHARAN, INFANTRY, "North African Warrior", 0, 0, 2, 2, 1, 1),
	MAMLUK_ARCHER(1, MUSLIM, INFANTRY, "Muslim Archer", 0, 0, 1, 1, 0, 1),
	MAMLUK_CAVALRY_CHARGE(1, MUSLIM, CAVALRY, "Charge Cavalry", 0, 0, 2, 0, 1, 1),
	MAMLUK_DUEL(5, MUSLIM, INFANTRY, "Muslim Duel Infantry", 0, 0, 1, 1, 1, 1),
	MAMLUK_MUSKET_CHARGE(18, MUSLIM, CAVALRY, "Musket Charge Cavalry", 1, 1, 4, 3, 3, 2),
	MANCHU_BANNER(17, CHINESE, CAVALRY, "Banner Cavalry", 1, 1, 3, 3, 3, 3),
	MAYA_FOREST_WARFARE(10, MESOAMERICAN, INFANTRY, "Reformed American Forest Warriors", 0, 0, 2, 2, 2, 2),
	MAYA_GUERILLA_WARFARE(19, MESOAMERICAN, INFANTRY, "American Guerrilla Warfare", 3, 2, 3, 2, 2, 2),
	MAYA_GUNPOWDER_WARFARE(14, MESOAMERICAN, INFANTRY, "American Forest Musketeers", 2, 2, 2, 2, 2, 2),
	MAYA_TRIBAL_WARFARE(5, MESOAMERICAN, INFANTRY, "American Forest Warriors", 0, 0, 2, 1, 1, 2),
	MESOAMERICAN_SPEARMEN(1, MESOAMERICAN, INFANTRY, "Mesoamerican Spearmen", 0, 0, 1, 0, 1, 1),
	MEXICAN_GUERILLA_WARFARE(19, MESOAMERICAN, INFANTRY, "Central American Guerrillas", 2, 2, 2, 2, 3, 3),
	MIXED_ORDER_INFANTRY(30, WESTERN, INFANTRY, "Mixed Order Infantry", 4, 3, 4, 3, 4, 4),
	MONGOL_BOW(1, NOMAD, INFANTRY, "Eastern Archers", 0, 0, 1, 1, 1, 1),
	MONGOL_STEPPE(1, NOMAD, CAVALRY, "Eastern Steppe Cavalry", 0, 0, 1, 1, 2, 2),
	MONGOL_SWARM(1, NOMAD, CAVALRY, "Eastern Swarm Cavalry", 0, 0, 2, 1, 2, 1),
	MONGOLIAN_BOW(1, CHINESE, CAVALRY, "Archer Cavalry", 0, 0, 0, 1, 1, 1),
	MUGHAL_MANSABDAR(6, INDIAN, CAVALRY, "Indian Cavalry Archers", 0, 0, 2, 2, 1, 1),
	MUGHAL_MUSKETEER(9, INDIAN, INFANTRY, "Toofangchis", 1, 1, 1, 1, 2, 1),
	MUSCOVITE_CARACOLLE(14, EASTERN, CAVALRY, "Eastern Caracole", 2, 0, 2, 2, 3, 3),
	MUSCOVITE_COSSACK(22, EASTERN, CAVALRY, "Cossack Cavalry", 1, 1, 4, 3, 4, 4),
	MUSCOVITE_MUSKETEER(12, EASTERN, INFANTRY, "Offensive Eastern Musketeers", 2, 1, 2, 1, 3, 2),
	MUSCOVITE_SOLDATY(15, EASTERN, INFANTRY, "Soldaty Infantry", 3, 1, 2, 1, 3, 2),
	MUSLIM_CAVALRY_ARCHERS(1, MUSLIM, CAVALRY, "Muslim Cavalry Archers", 0, 0, 1, 1, 1, 1),
	MUSLIM_DRAGOON(23, MUSLIM, CAVALRY, "Muslim Dragoon", 1, 2, 3, 4, 3, 4),
	MUSLIM_MASS_INFANTRY(26, MUSLIM, INFANTRY, "Muslim Mass Infantry", 3, 4, 3, 3, 3, 3),
	NAPOLEONIC_LANCERS(28, WESTERN, CAVALRY, "Latin Lancers", 0, 2, 6, 4, 5, 4),
	NAPOLEONIC_SQUARE(30, WESTERN, INFANTRY, "Napoleonic Square", 4, 4, 4, 3, 4, 3),
	NATIVE_CLUBMEN(1, NORTH_AMERICAN, INFANTRY, "Clubmen", 0, 0, 1, 0, 1, 1),
	NATIVE_INDIAN_ARCHER(1, NORTH_AMERICAN, INFANTRY, "Native American Archer", 0, 0, 0, 1, 1, 1),
	NATIVE_INDIAN_HORSEMEN(6, NORTH_AMERICAN, CAVALRY, "North American Horsemen", 0, 0, 2, 1, 1, 1),
	NATIVE_INDIAN_MOUNTAIN_WARFARE(10, NORTH_AMERICAN, INFANTRY, "Native American Mountain Warriors", 1, 1, 1, 2, 2, 1),
	NATIVE_INDIAN_TRIBAL_WARFARE(5, NORTH_AMERICAN, INFANTRY, "American Plains Warriors", 0, 0, 2, 1, 1, 2),
	NIGER_KONGOLESE_FOREST_WARFARE(12, SUB_SAHARAN, INFANTRY, "African Forest Warriors", 1, 1, 2, 2, 2, 2),
	NIGER_KONGOLESE_GUERILLA_WARFARE(23, SUB_SAHARAN, INFANTRY, "Central African Guerrillas", 3, 2, 2, 3, 3, 2),
	NIGER_KONGOLESE_GUNPOWDER_WARFARE(15, SUB_SAHARAN, INFANTRY, "Central African Musketeers", 3, 2, 2, 2, 2, 2),
	NIGER_KONGOLESE_TRIBAL_WARFARE(5, SUB_SAHARAN, INFANTRY, "Central African Warrior", 0, 0, 1, 1, 2, 2),
	NORTH_AMERICAN_HUSSAR(14, NORTH_AMERICAN, CAVALRY, "North American Hussar", 1, 1, 3, 2, 3, 2),
	NORTH_AMERICAN_RIFLE_CAVALRY(10, NORTH_AMERICAN, CAVALRY, "North American Cavalry", 0, 1, 3, 2, 2, 2),
	OPEN_ORDER_CAVALRY(28, WESTERN, CAVALRY, "Latin Chasseur", 2, 1, 4, 4, 5, 5),
	OTTOMAN_AZAB(5, OTTOMAN, INFANTRY, "Azab Infantry", 0, 0, 1, 1, 2, 1),
	OTTOMAN_JANISSARY(9, OTTOMAN, INFANTRY, "Janissary Infantry", 1, 0, 1, 1, 2, 2),
	OTTOMAN_LANCER(28, OTTOMAN, CAVALRY, "Reformed Lancer", 1, 1, 5, 3, 5, 3),
	OTTOMAN_MUSELLEM(1, OTTOMAN, CAVALRY, "Musellem Cavalry", 0, 0, 2, 1, 1, 1),
	OTTOMAN_NEW_MODEL(30, OTTOMAN, INFANTRY, "Eastern New Model Infantry", 3, 3, 3, 3, 4, 4),
	OTTOMAN_NIZAMI_CEDID(23, OTTOMAN, INFANTRY, "Nizami Cedid Infantry", 2, 3, 2, 2, 3, 3),
	OTTOMAN_REFORMED_JANISSARY(19, OTTOMAN, INFANTRY, "Reformed Janissary Infantry", 2, 2, 2, 2, 3, 2),
	OTTOMAN_REFORMED_SPAHI(18, OTTOMAN, CAVALRY, "Reformed Spahi Cavalry", 1, 1, 3, 3, 3, 3),
	OTTOMAN_SEKBAN(12, OTTOMAN, INFANTRY, "Sekban Infantry", 2, 2, 2, 1, 2, 3),
	OTTOMAN_SPAHI(10, OTTOMAN, CAVALRY, "Spahi Cavalry", 0, 0, 3, 2, 3, 2),
	OTTOMAN_TIMARIOT(6, OTTOMAN, CAVALRY, "Timariot Cavalry", 0, 0, 2, 1, 2, 1),
	OTTOMAN_TOPRAKLI_DRAGOON(28, OTTOMAN, CAVALRY, "Toprakli Dragoons", 2, 2, 3, 3, 4, 4),
	OTTOMAN_TOPRAKLI_HIT_AND_RUN(23, OTTOMAN, CAVALRY, "Toprakli Hit and Run Cavalry", 2, 0, 4, 3, 4, 3),
	OTTOMAN_YAYA(1, OTTOMAN, INFANTRY, "Yaya Infantry", 0, 0, 1, 0, 1, 1),
	PEDRERO(10, WESTERN, ARTILLERY, "Pedrero", 1, 1, 0, 0, 1, 1),
	PERSIAN_CAVALRY_CHARGE(1, MUSLIM, CAVALRY, "Muslim Cavalry", 0, 0, 1, 0, 2, 1),
	PERSIAN_FOOTSOLDIER(1, MUSLIM, INFANTRY, "Muslim Foot Soldier", 0, 0, 1, 0, 1, 1),
	PERSIAN_RIFLE(30, MUSLIM, INFANTRY, "Muslim Rifle Infantry", 3, 3, 3, 3, 4, 4),
	PERSIAN_SHAMSHIR(9, MUSLIM, INFANTRY, "Shamshir Infantry", 0, 0, 2, 1, 2, 2),
	PERUVIAN_GUERILLA_WARFARE(19, SOUTH_AMERICAN, INFANTRY, "South American Guerrilla", 2, 2, 2, 2, 3, 3),
	POLISH_HUSSAR(14, EASTERN, CAVALRY, "Reformed Eastern Hussars", 1, 1, 3, 3, 2, 2),
	POLISH_MUSKETEER(12, EASTERN, INFANTRY, "Defensive Eastern Musketeers", 2, 2, 1, 2, 2, 2),
	POLISH_TERCIO(15, EASTERN, INFANTRY, "Eastern Tercio", 2, 2, 1, 2, 2, 3),
	POLISH_WINGED_HUSSAR(22, EASTERN, CAVALRY, "Winged Hussars", 0, 1, 5, 4, 4, 3),
	PRUSSIAN_DRILL(30, WESTERN, INFANTRY, "Drill Infantry", 4, 4, 3, 3, 4, 4),
	PRUSSIAN_FREDERICKIAN(26, WESTERN, INFANTRY, "Frederickian Infantry", 4, 3, 3, 3, 4, 3),
	PRUSSIAN_UHLAN(26, WESTERN, CAVALRY, "Uhlan Cavalry", 1, 2, 5, 4, 4, 3),
	PUEBLO_AMBUSH(5, NORTH_AMERICAN, INFANTRY, "Ambush Infantry", 0, 0, 1, 2, 2, 1),
	QIZILBASH_CAVALRY(10, MUSLIM, CAVALRY, "Qizilbash Cavalry", 0, 0, 2, 2, 3, 2),
	RAJPUT_HILL_FIGHTERS(1, INDIAN, CAVALRY, "Indian Cavalry", 0, 0, 0, 1, 1, 1),
	RAJPUT_MUSKETEER(12, INDIAN, INFANTRY, "Akbarid Musketeers", 2, 2, 1, 2, 2, 2),
	REFORMED_ASIAN_CAVALRY(28, CHINESE, CAVALRY, "Reformed Asian Cavalry", 1, 2, 5, 4, 3, 4),
	REFORMED_ASIAN_MUSKETEER(26, CHINESE, INFANTRY, "Reformed Asian Musketeer", 4, 4, 3, 3, 3, 3),
	REFORMED_MANCHU_RIFLE(28, CHINESE, CAVALRY, "Green Standard Cavalry", 2, 1, 4, 4, 4, 4),
	REFORMED_MUGHAL_MANSABDAR(14, INDIAN, CAVALRY, "Dai-Phat Cavalry", 2, 0, 2, 3, 3, 2),
	REFORMED_MUGHAL_MUSKETEER(12, INDIAN, INFANTRY, "South Indian Infantry", 2, 1, 2, 1, 3, 2),
	REFORMED_STEPPE_RIFLES(30, NOMAD, INFANTRY, "Reformed Steppe Rifles", 3, 3, 3, 3, 4, 4),
	REFORMED_WESTERNIZED_INCAN(30, SOUTH_AMERICAN, INFANTRY, "Reformed Westernized Incan Infantry", 4, 4, 3, 3, 3, 3),
	ROYAL_MORTAR(25, WESTERN, ARTILLERY, "Royal Mortar", 4, 4, 1, 1, 3, 3),
	RUSSIAN_COSSACK(28, EASTERN, CAVALRY, "Advanced Cossack Cavalry", 2, 1, 4, 4, 5, 4),
	RUSSIAN_CUIRASSIER(28, EASTERN, CAVALRY, "Eastern Cuirassiers", 1, 2, 5, 4, 4, 4),
	RUSSIAN_GREEN_COAT(26, EASTERN, INFANTRY, "Green Coat Infantry", 3, 3, 3, 3, 4, 4),
	RUSSIAN_LANCER(26, EASTERN, CAVALRY, "Lancers", 0, 1, 5, 4, 4, 4),
	RUSSIAN_MASS(30, EASTERN, INFANTRY, "Mass Infantry", 4, 3, 4, 3, 4, 3),
	RUSSIAN_PETRINE(23, EASTERN, INFANTRY, "Petrine Infantry", 2, 3, 2, 3, 3, 3),
	SAXON_INFANTRY(19, EASTERN, INFANTRY, "Saxon Infantry", 3, 2, 3, 2, 2, 2),
	SCHWARZE_REITER(10, WESTERN, CAVALRY, "Schwarze Reiter", 1, 0, 2, 1, 2, 2),
	SCOTTISH_HIGHLANDER(19, WESTERN, INFANTRY, "Highlanders Infantry", 2, 2, 3, 2, 4, 2),
	SHAYBANI(6, MUSLIM, CAVALRY, "Shaybanid Cavalry", 0, 0, 2, 1, 1, 2),
	SIKH_HIT_AND_RUN(18, INDIAN, INFANTRY, "North Indian Musket Infantry", 3, 2, 2, 2, 3, 2),
	SIKH_RIFLE(28, INDIAN, CAVALRY, "Mysorean Light Cavalry", 2, 1, 4, 4, 3, 4),
	SIOUX_DRAGOON(23, NORTH_AMERICAN, CAVALRY, "North American Dragoon", 2, 2, 3, 3, 4, 4),
	SLAVIC_STRADIOTI(6, EASTERN, CAVALRY, "Stratioti Cavalry", 0, 0, 2, 1, 1, 1),
	SMALL_CAST_IRON_BOMBARD(13, WESTERN, ARTILLERY, "Small Cast Iron Cannon", 1, 1, 0, 1, 2, 1),
	SONGHAI_TRIBAL_WARFARE(5, SUB_SAHARAN, INFANTRY, "West African Warrior", 0, 0, 1, 2, 1, 2),
	SOUTH_AMERICAN_ARQUEBUSIER(10, SOUTH_AMERICAN, INFANTRY, "South American Arquebusier", 1, 2, 1, 1, 2, 1),
	SOUTH_AMERICAN_DRAGOON(23, SOUTH_AMERICAN, CAVALRY, "South American Dragoon", 2, 2, 3, 3, 4, 4),
	SOUTH_AMERICAN_FOREST_WARFARE(5, SOUTH_AMERICAN, INFANTRY, "South American Forest Warriors", 0, 0, 1, 2, 2, 1),
	SOUTH_AMERICAN_GUNPOWDER_WARFARE(10, SOUTH_AMERICAN, INFANTRY, "Defensive American Musketeers", 1, 1, 1, 2, 1, 2),
	SOUTH_AMERICAN_HORSEMEN(6, SOUTH_AMERICAN, CAVALRY, "South American Horsemen", 0, 0, 2, 1, 1, 1),
	SOUTH_AMERICAN_HUSSAR(14, SOUTH_AMERICAN, CAVALRY, "South American Hussar", 1, 1, 3, 2, 3, 2),
	SOUTH_AMERICAN_REFORMED_GUNPOWDER_WARFARE(14, SOUTH_AMERICAN, INFANTRY, "Reformed American Musketeers", 2, 2, 2, 2, 2, 2),
	SOUTH_AMERICAN_RIFLE_CAVALRY(10, SOUTH_AMERICAN, CAVALRY, "South American Cavalry", 0, 1, 3, 2, 2, 2),
	SOUTH_AMERICAN_SPEARMEN(1, SOUTH_AMERICAN, INFANTRY, "South American Spearmen", 0, 0, 1, 1, 1, 0),
	SOUTH_AMERICAN_SWARM(19, SOUTH_AMERICAN, CAVALRY, "South American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
	SOUTH_AMERICAN_WARFARE(1, SOUTH_AMERICAN, INFANTRY, "American Mountain Warriors", 0, 0, 0, 1, 1, 1),
	SOUTH_INDIAN_MUSKETEER(9, INDIAN, INFANTRY, "Poligar Infantry", 1, 2, 1, 1, 1, 1),
	SPANISH_TERCIO(12, WESTERN, INFANTRY, "Tercio Infantry", 1, 2, 1, 2, 2, 2),
	STEPPE_CAVALRY(23, NOMAD, CAVALRY, "Steppe Cavalry", 1, 1, 4, 3, 3, 3),
	STEPPE_FOOTMEN(12, NOMAD, INFANTRY, "Steppe Footmen", 1, 1, 2, 2, 2, 2),
	STEPPE_INFANTRY(19, NOMAD, INFANTRY, "Steppe Infantry", 2, 2, 3, 3, 3, 2),
	STEPPE_LANCERS(14, NOMAD, CAVALRY, "Steppe Lancers", 0, 0, 3, 3, 3, 3),
	STEPPE_MOUNTED_RAIDERS(18, NOMAD, CAVALRY, "Mounted Steppe Raiders", 0, 1, 3, 3, 3, 3),
	STEPPE_MUSKETEERS(15, NOMAD, INFANTRY, "Steppe Musketeers", 2, 1, 3, 2, 2, 2),
	STEPPE_RAIDERS(9, NOMAD, INFANTRY, "Steppe Raiders", 0, 0, 2, 1, 1, 2),
	STEPPE_RIDERS(10, NOMAD, CAVALRY, "Steppe Riders", 0, 0, 3, 2, 3, 2),
	STEPPE_RIFLES(26, NOMAD, INFANTRY, "Steppe Rifles", 2, 3, 3, 3, 4, 4),
	STEPPE_UHLANS(28, NOMAD, CAVALRY, "Steppe Uhlans", 1, 1, 5, 4, 4, 3),
	SWEDISH_ARME_BLANCHE(23, WESTERN, CAVALRY, "Arme Blanche Cavalry", 1, 1, 5, 3, 5, 3),
	SWEDISH_CAROLINE(23, WESTERN, INFANTRY, "Caroline Infantry", 3, 2, 3, 2, 3, 3),
	SWEDISH_GALLOP(18, WESTERN, CAVALRY, "Gallop Cavalry", 1, 1, 4, 3, 3, 3),
	SWEDISH_GUSTAVIAN(19, WESTERN, INFANTRY, "Gustavian Infantry", 3, 2, 3, 2, 3, 2),
	SWISS_LANDSKNECHTEN(9, WESTERN, INFANTRY, "Landsknechten Infantry", 0, 0, 1, 1, 1, 2),
	SWIVEL_CANNON(20, WESTERN, ARTILLERY, "Swivel Cannon", 3, 2, 1, 1, 3, 2),
	TARTAR_COSSACK(26, EASTERN, CAVALRY, "Reformed Cossack Cavalry", 1, 1, 4, 4, 4, 4),
	TIPU_SULTAN_ROCKET(30, INDIAN, INFANTRY, "Indian Drill Infantry", 4, 3, 3, 3, 4, 3),
	TOFONGCHIS_MUSKETEER(12, MUSLIM, INFANTRY, "Muslim Musketeer", 2, 1, 1, 2, 2, 3),
	TOPCHIS_ARTILLERY(14, MUSLIM, CAVALRY, "Muslim Musketeer Cavalry", 1, 0, 2, 2, 3, 2),
	WESTERN_LONGBOW(5, WESTERN, INFANTRY, "Longbow", 0, 0, 1, 0, 1, 1),
	WESTERN_MEDIEVAL_INFANTRY(1, WESTERN, INFANTRY, "Latin Medieval Infantry", 0, 0, 0, 0, 1, 1),
	WESTERN_MEDIEVAL_KNIGHTS(1, WESTERN, CAVALRY, "Latin Knights", 0, 0, 1, 0, 1, 1),
	WESTERN_MEN_AT_ARMS(5, WESTERN, INFANTRY, "Men at Arms", 0, 0, 0, 1, 1, 1),
	WESTERNIZED_ADAL(26, SUB_SAHARAN, INFANTRY, "Westernized East African Infantry", 4, 4, 2, 2, 3, 4),
	WESTERNIZED_AZTEC(26, MESOAMERICAN, INFANTRY, "Westernized American Hill Infantry", 3, 4, 2, 3, 3, 4),
	WESTERNIZED_BANTU(26, SUB_SAHARAN, INFANTRY, "Westernized South African Infantry", 4, 3, 3, 3, 3, 3),
	WESTERNIZED_ETHIOPIAN(26, SUB_SAHARAN, INFANTRY, "Westernized North African Infantry", 3, 4, 2, 4, 3, 3),
	WESTERNIZED_INCAN(26, SOUTH_AMERICAN, INFANTRY, "Westernized Incan Infantry", 4, 3, 3, 3, 3, 3),
	WESTERNIZED_MAYAN(30, MESOAMERICAN, INFANTRY, "Westernized Central American infantry", 4, 3, 3, 3, 3, 4),
	WESTERNIZED_NIGER_KONGOLESE(26, SUB_SAHARAN, INFANTRY, "Westernized Central African Infantry", 3, 3, 3, 3, 4, 3),
	WESTERNIZED_SOUTH_AMERICAN(26, SOUTH_AMERICAN, INFANTRY, "Westernized South American Infantry", 3, 4, 2, 3, 3, 4),
	WESTERNIZED_ZAPOTEC(26, MESOAMERICAN, INFANTRY, "Westernized American Plains Infantry", 4, 3, 4, 3, 3, 2),
	ZAPOROGHIAN_COSSACK(14, EASTERN, CAVALRY, "Southern Cossacks", 0, 1, 4, 2, 2, 3),
	ZAPOTEC_GUNPOWDER_WARFARE(14, MESOAMERICAN, INFANTRY, "Offensive American Musketeers", 2, 1, 1, 2, 3, 3),
	ZAPOTEC_PLAINS_WARFARE(10, MESOAMERICAN, INFANTRY, "Reformed American Plains Warriors", 0, 0, 1, 2, 3, 2),
	ZAPOTEC_TRIBAL_WARFARE(5, MESOAMERICAN, INFANTRY, "American Plains Warriors", 0, 0, 1, 1, 2, 2);
	
	/**
	 * Military technology level the unit becomes available (in the range of 0-32)
	 */
	public final int tech_level;
	/**
	 * {@link TechnologyGroup} of the Unit. All artillery will be considered WESTERN
	 * tech by convention (it is available to all tech groups).
	 */
	public final Technology.Group tech_group;
	/** Type of the Unit, one of INFANTRY, CAVALRY, ARTILLERY */
	public final UnitType type;
	/** Name of the unit, such as "Western Longbow" */
	@Getter public final String name;
	/** Offensive fire pips (0-6) */
	public final int off_fire;
	/** Defensive fire pips (0-6) */
	public final int def_fire;
	/** Offensive shock pips (0-6) */
	public final int off_shock;
	/** Defensive shock pips (0-6) */
	public final int def_shock;
	/** Offensive morale pips (0-6) */
	public final int off_morale;
	/** Defensive morale pips (0-6) */
	public final int def_morale;
	
	/**
	 * @return Pips of the unit for the given phase (FIRE/SHOCK/MORALE) and combat
	 *         type (OFFENSE/DEFENSE)
	 */
	public int getPips(Phase phase, CombatType combatType) {
		if (combatType == CombatType.OFFENSE && phase == Phase.FIRE)
			return off_fire;
		if (combatType == CombatType.DEFENSE && phase == Phase.FIRE)
			return def_fire;
		if (combatType == CombatType.OFFENSE && phase == Phase.SHOCK)
			return off_shock;
		if (combatType == CombatType.DEFENSE && phase == Phase.SHOCK)
			return def_shock;
		if (combatType == CombatType.OFFENSE && phase == Phase.FIRE)
			return off_fire;
		if (combatType == CombatType.DEFENSE && phase == Phase.FIRE)
			return def_fire;
		if (combatType == CombatType.OFFENSE && phase == Phase.MORALE)
			return off_morale;
		if (combatType == CombatType.DEFENSE && phase == Phase.MORALE)
			return def_morale;
		return 0;
	}
	
	/**
	 * Convenience method to find a Unit of the given name. If no unit of the exact
	 * same name exists, <code>null</code> is returned
	 */
	public static Unit fromString(String name) {
		return Stream.of(Unit.values()).filter(u -> u.name.equalsIgnoreCase(name)).findAny().orElse(null);
	}
	
	public String getTooltip() {
		StringBuilder sb = new StringBuilder();
		// sb.append("(Pips: (off. fire,def. fire,off. shock, def. shock, off.
		// morale, def. morale): \n");
		sb.append("Off. Fire: \t" + off_fire + "\n");
		sb.append("Def. Fire: \t" + def_fire + "\n");
		sb.append("Off. Shock: \t" + off_shock + "\n");
		sb.append("Def. Shock: \t" + def_shock + "\n");
		sb.append("Off. Morale: \t" + off_morale + "\n");
		sb.append("Def. Morale: \t" + def_morale + "\n");
		sb.append("Total Pips: \t" + Integer.toString(off_fire + def_fire + off_shock + def_shock + off_morale + def_morale) + "\n");
		
		return sb.toString();
	}
}
