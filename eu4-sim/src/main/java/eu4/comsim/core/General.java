package eu4.comsim.core;

import java.io.Serializable;

import eu4.comsim.core.datatypes.Phase;
import lombok.Value;

/**
 * Models a EU4 land leader
 *
 * @see <a href="http://www.eu4wiki.com/Military_leader">http://www.eu4wiki.com/
 *      Military_leader</a>
 */
@Value
public class General implements Serializable {
	
	private static final long serialVersionUID = -7104995321796196883L;
	
	/** Fire pips (0-6), applied in {@link Phase#FIRE} */
	int fire;
	/** Shock pips (0-6), applied in {@link Phase#SHOCK} */
	int shock;
	/** Maneuver pips (0-6), applied in determining attacker crossing penalty */
	int maneuver;
	/** Siege pips (0-6), not relevant directly in combat */
	int siege;
	
	/**
	 * Pips for fire or shock. Not sensible to pass {@link Phase#MORALE} here
	 */
	public int getPips(Phase phase) {
		if (phase == Phase.MORALE)
			throw new IllegalArgumentException("Only fire and shock allowed");
		return (phase == Phase.FIRE) ? fire : shock;
	}
	
	/**
	 * Computes die modifier based on General stats for relevant phase and terrain
	 * penalty
	 *
	 * @param opponent Opponent general
	 * @param phase    Current phase
	 * @return Difference between general phase pips
	 */
	public int difference(General opponent, Phase phase) {
		int generalDifference = Math.max(0, getPips(phase) - opponent.getPips(phase));
		return generalDifference;
	}
}
