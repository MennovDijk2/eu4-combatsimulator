package eu4.comsim.core;

import lombok.Value;

@Value
public final class BattleUpdateEvent {
	
	int day;
	Battle battle;
	
	public BattleStats getStats(boolean attacker) {
		Battleline battleline = (attacker) ? battle.battleLineA : battle.battleLineB;
		return battleline.history.get(day);
	}
}